var gulp = require('gulp');
var runSequence = require('run-sequence');
var replace = require('gulp-replace');

var sass = require('gulp-sass');
var postcss = require('gulp-postcss');
var autoprefixer = require('autoprefixer');
var minifyCss = require('gulp-minify-css');

var webfonts = require('gulp-google-webfonts');

gulp.task('css', function () {

    return gulp.src('src/**/!(_)*.scss')
        .pipe(sass())
        .pipe(replace('fonts/', '../fonts/'))
        .pipe(postcss([
            autoprefixer({ browsers: ['> 0.01%'] })
        ]))
        .pipe(minifyCss())
        .pipe(gulp.dest('dist'));
});

gulp.task('web-fonts', function () {

    return gulp.src('fonts.list')
        .pipe(webfonts({
            fontsDir: 'fonts',
            cssDir: 'css/base',
            cssFilename: '_fonts.scss'
        }))
        .pipe(gulp.dest('src'));
});

gulp.task('fonts', function () {

    return gulp.src('src/fonts/*.*')
        .pipe(gulp.dest('dist/fonts'));
});

gulp.task('images', function () {

    return gulp.src('src/images/**/*.png')
        .pipe(gulp.dest('dist/images'));
});

gulp.task('watch', function () {

    gulp.watch('src/css/**/*.scss', ['css']);

    gulp.watch('src/fonts/*.*', function () {
        runSequence('fonts', 'css');
    });

    gulp.watch('fonts.list', ['web-fonts']);

    gulp.watch('src/images/**/*.png', ['images']);
});

gulp.task('default', function (callback) {

    runSequence('web-fonts', 'fonts', 'images', 'css', callback);
});